import json
import os
from logging import getLogger
from pathlib import Path
from sys import stderr
from typing import Callable, Union, Optional
from warnings import warn
import requests
from requests.adapters import HTTPAdapter
from urllib3.util import Retry
import pandas as pd
import numpy as np
import joblib as jb
from tqdm import trange


_LOGGER = getLogger().getChild(__name__)

# system CA certificates
_SYS_CA_BUNDLE = '/etc/ssl/certs/ca-certificates.crt'
_ENV_CA_BUNDLES = ['REQUESTS_CA_BUNDLE', 'CURL_CA_BUNDLE']

# requests timeouts
_CONNECT_TIMEOUT = 3.05
_READ_TIMEOUT = 60.05

# max retries for requests
_MAX_RETRIES = 5
_RETRIES_BACKOFF_FACTOR = 1

# kwargs for tqdm bars
# file is used for progress messages as well
_PROGRESS_KWARGS = {
  'mininterval': 5, 'maxinterval': 5, 'file': stderr, 'dynamic_ncols': True
}

# fields names used in output dataframes of the project
_LOCAL_FIELDS = {

  # timestamps
  'epoch': 'epoch',

  # TLE
  'tle_line1': 'tle_line1',
  'tle_line2': 'tle_line2',

  # AMS gps
  'x': 'x',
  'y': 'y',
  'z': 'z',
  'latitude': 'latitude',
  'longitude': 'longitude',
  'altitude': 'altitude',
  'velocity_x': 'velocity_x',
  'velocity_y': 'velocity_y',
  'velocity_z': 'velocity_z',

  # AMS flight
  'pitch': 'pitch_angle',
  'roll': 'roll_angle',
  'yaw': 'yaw_angle',

  # AMS fast trigger
  'livetime': 'livetime',
  'LV1': 'LV1',
  'FT': 'FT',
  'FTZ': 'FTZ',
  'FTC': 'FTC',
  'FTE': 'FTE',
  'subLV1_0': 'subLV1_0',
  'subLV1_1': 'subLV1_1',
  'subLV1_2': 'subLV1_2',
  'subLV1_3': 'subLV1_3',
  'subLV1_4': 'subLV1_4',
  'subLV1_5': 'subLV1_5',
  'subLV1_6': 'subLV1_6',

  # L-values
  'L': 'L',

  # geomagnetic field
  'B': 'B',

  # zenith angles
  'zenith': 'zenith',

  # age of position and orientation data
  'pos_age': 'pos_age',
  'orient_age': 'orient_age',
}

_RATES_SUFFIXES = {
  'corr': 'corr',   # corrected form (i.e. rate / livetime)
  'ratio': 'ratio', # ratio (i.e. rate_1 / rate_2)
}


# log module's global variables
_LOGGER.debug(f'system CA bundle: {_SYS_CA_BUNDLE}')
_LOGGER.debug(f'env variables for CA bundles: {_ENV_CA_BUNDLES}')
_LOGGER.debug(f'connect timeout for requests: {_CONNECT_TIMEOUT}')
_LOGGER.debug(f'read timeout for requests: {_READ_TIMEOUT}')
_LOGGER.debug(f'max retries for requests: {_MAX_RETRIES}')
_LOGGER.debug(f'requests retries backoff factor: {_RETRIES_BACKOFF_FACTOR}')
_LOGGER.debug(f'progress bars additional kwargs: {_PROGRESS_KWARGS}')
_LOGGER.debug(f'local fields: {json.dumps(_LOCAL_FIELDS, indent = 2)}')
_LOGGER.debug(f'rates suffixes: {json.dumps(_RATES_SUFFIXES, indent = 2)}')


class mySession(requests.Session):
  f'''
  Custom `requests.Session` class.

  The additions made to the standard class are:
  - trust the environment, to use `HTTP_PROXY` and `HTTPS_PROXY` env variables
  - load system CA certificates for ssl
  - default connect and read timeouts for requests (i.e. {_CONNECT_TIMEOUT} and
    {_READ_TIMEOUT} seconds)
  - {_MAX_RETRIES} max retries

  Objects of this class can be used like any other requests object.
  '''

  _logger = _LOGGER.getChild('classMySession')

  def __init__(self, *args, **kwargs):

    logger = self._logger.getChild('__init__')

    super(mySession, self).__init__(*args, **kwargs)

    # read parameters from environment (e.g. proxies)
    self.trust_env = True

    # ssl certificates

    env_bundle = False

    for var in _ENV_CA_BUNDLES:
      if os.getenv(var) is not None:
        env_bundle = True
        break

    if env_bundle:
      logger.debug('CA bundles read from env: {}'.format(
        {var: os.getenv(var) for var in _ENV_CA_BUNDLES}
      ))

    # load sys bundle
    if Path(_SYS_CA_BUNDLE).exists():
      self.verify = _SYS_CA_BUNDLE
    else:
      logger.debug('unable to load system CA certs')

      if not env_bundle:
        warn(
          'Unable to load system SSL certificates. Please provide a CA ' + \
            'bundle setting REQUEST_CA_BUNDLE env variable'
        )

    # max retries
    for prefix in ['http://', 'https://']:
      self.mount(prefix, HTTPAdapter(max_retries = Retry(
        _MAX_RETRIES, backoff_factor = _RETRIES_BACKOFF_FACTOR,
        allowed_methods = list(Retry.DEFAULT_ALLOWED_METHODS) + ['POST']
      )))


  def request(self, *args, **kwargs) -> requests.Response:

    logger = self._logger.getChild('request')

    if 'timeout' not in kwargs:
      kwargs['timeout'] = (_CONNECT_TIMEOUT, _READ_TIMEOUT)
      logger.debug(f'default timeouts inserted in kwargs: {kwargs["timeout"]}')

    return super(mySession, self).request(*args, **kwargs)


def _get_progress_kwargs(user_kwargs: Optional[dict] = None) -> dict:
  '''
  Merge user and project's default kwargs for tqdm objects.

  Parameters:
  - user_kwargs: user-defined kwargs for `tqdm` progress bars

  Returns:
  updated kwargs with project's predefined kwargs; user's args have the priority
  over project's ones.
  '''

  logger = _LOGGER.getChild('_get_progress_kwargs')

  logger.debug(f'args: user_kwargs: {user_kwargs}')

  kwargs = _PROGRESS_KWARGS.copy()

  if user_kwargs is not None:
    kwargs.update(user_kwargs)

  logger.debug(f'progress kwargs (return): {kwargs}')

  return kwargs


def parallel_apply(
  df: pd.DataFrame, func: Callable, n_jobs: int = -1,
  progress_kwargs: Optional[dict] = None
) -> Union[pd.Series, pd.DataFrame]:
  '''
  Apply a function along rows of a dataframe in parallel.

  Parameters:
  - df: input dataframe
  - func: function to apply
  - n_jobs: parallel jobs (default: nproc; look at `joblib.Parallel` reference
    for more info)
  - progress_kwargs: kwargs for progress bar (i.e. `tqdm.trange` object)

  Returns:
  series or dataframe, depending on the shape of the result
  '''

  logger = _LOGGER.getChild('parallel_apply')

  ## args

  logger.debug(
    f'args: dataframe shape: {df.shape}, func: {func}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  ## apply

  x = np.array(jb.Parallel(n_jobs = n_jobs)(
    jb.delayed(func)(*df.iloc[i].tolist())
    for i in trange(df.shape[0], **_get_progress_kwargs(progress_kwargs))
  ))

  x = pd.DataFrame(x) if x.ndim > 1 else pd.Series(x)
  x.index = df.index

  logger.debug(f'output shape (return): {x.shape}')

  return x


def compact_records(df: pd.DataFrame) -> pd.DataFrame:
  '''
  Compact data records, e.g.:

    index - col1 - col2        index - col1 - col2
        i -   x1 -  nan   -->      i -   x1 -   x2
        i -  nan -   x2

  Parameters:
  - df: input dataframe

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('compact_records')

  logger.debug(f'args: dataframe shape: {df.shape}')

  if df.shape[0] == 0:
    logger.debug(f'dataframe is empty, return itself')
    return df

  # reset index
  df = df.reset_index()
  idx = df.columns.tolist()[0]

  ## compact

  df = df.melt(id_vars = idx)

  # look for duplicates
  duplicates = df.loc[df.duplicated([idx, 'variable'])]

  if duplicates.shape[0] > 0:

    duplicates = duplicates.pivot(
      index = idx, columns = 'variable', values = 'value'
    )
    desc = duplicates.reset_index().describe(datetime_is_numeric = True)

    # warn and log duplicates
    warn('dataframe to compact contains duplicates that will be dropped')
    logger.info(
      'Description of duplicates in dataframe to compact that will be '
      f'dropped: \n{desc.to_string()}'
    )

    # drop duplicates
    df = df.drop_duplicates([idx, 'variable'])

  df = df.pivot(index = idx, columns = 'variable', values = 'value')

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df
