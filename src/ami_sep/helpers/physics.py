'''
Physics helper functions.
'''


from logging import getLogger
import pandas as pd
import numpy as np
from numpy import linalg
from spacepy.time import Ticktock
from spacepy.coordinates import Coords
from spacepy.irbempy import get_Lm, get_Bfield
from scipy.spatial.transform import Rotation as R


_LOGGER = getLogger().getChild(__name__)

_EARTH_RADIUS = 6371200 # m
_AMS_ROTVEC = np.deg2rad(-12.0001) * np.array([1,0,0]) # rotor vector in radians

_LOGGER.debug(f'Earth radius: {_EARTH_RADIUS} m')
_LOGGER.debug(
  f'AMS rotvec (in respect to the ISS ref. system): {_AMS_ROTVEC} rad'
)


def _get_spacepy_coords(x: float, y: float, z: float) -> Coords:
  '''
  Get Coords object from cartesian GEO coordinates.

  Parameters:
  - x, y, z: cartesian position components (units: Earth radii)

  Returns:
  SpacePy Coords object
  '''

  return Coords([[x,y,z]], 'GEO', 'car', use_irbem = False)


def _get_spacepy_time(t: pd.Timestamp) -> Ticktock:
  '''
  Get Ticktock object from timestamp.

  Parameters:
  - t: timestamp parsable by `pandas.Timestamp`

  Returns:
  Ticktock object
  '''

  t = pd.Timestamp(t)
  t = t.tz_localize('utc') if t.tz is None else t.tz_convert('utc')

  # time-aware timezones in ISO format could raise a fatal error in
  # Ticktock, so tz is removed
  t = Ticktock([t.tz_localize(None).isoformat()], 'ISO')

  return t


def get_earth_radii(x: float) -> float:
  '''
  Convert distance from meters to Earth radii.

  Parameters:
  - x: distance (units: m)

  Returns:
  distance (units: Earth radii)
  '''

  return x / _EARTH_RADIUS


def get_L(
  epoch: pd.Timestamp, x: float, y: float, z: float,
  alpha: float = 90.0, extMag: str = '0', intMag: str = 'IGRF'
) -> float:
  '''
  Wrapper for `spacepy.irbempy.get_Lm`.

  Parameters:
  - epoch: timestamp parsable by `pandas.Timestamp`
  - x, y, z: cartesian position components (units: Earth radii)
  - alpha: pitch angle (units: deg)
  - extMag: external magnetic field model (more info in `spacepy.irbempy.get_Lm`
    docs)
  - intMag: internal magnetic field model (more info in `spacepy.irbempy.get_Lm`
    docs)

  Returns:
  L absolute value (units: Earth radii)
  '''

  logger = _LOGGER.getChild('get_L')

  ## args

  logger.debug(
    f'args: epoch: {epoch}, x: {x}, y: {y}, z: {z}, alpha: {alpha}, '
    f'extMag: {extMag}, intMag: {intMag}'
  )

  ## L

  epoch = _get_spacepy_time(epoch)
  coordinates = _get_spacepy_coords(x,y,z)

  logger.debug(f'epoch: {epoch}, coordinates: {coordinates}')

  L_dict = get_Lm(
    ticks = epoch, loci = coordinates, alpha = [alpha], extMag = extMag,
    intMag = intMag
  )

  logger.debug(f'L dict: {L_dict}')

  # return L absolute value
  L = np.abs(L_dict['Lm'][0][0])

  logger.debug(f'L (return): {L}')

  return L


def get_B(
  epoch: pd.Timestamp, x: float, y: float, z: float, extMag: str = '0'
) -> float:
  '''
  Wrapper for `spacepy.irbempy.get_Bfield`.

  Parameters:
  - epoch: timestamp parsable by `pandas.Timestamp`
  - x, y, z: cartesian position components (units: Earth radii)
  - extMag: external magnetic field model (more info in
    `spacepy.irbempy.get_Bfield` docs)

  Returns:
  B value (units: nT)
  '''

  logger = _LOGGER.getChild('get_B')

  ## args

  logger.debug(
    f'args: epoch: {epoch}, x: {x}, y: {y}, z: {z}, extMag: {extMag}'
  )

  ## B

  epoch = _get_spacepy_time(epoch)
  coordinates = _get_spacepy_coords(x,y,z)

  logger.debug(f'epoch: {epoch}, coordinates: {coordinates}')

  B_dict = get_Bfield(ticks = epoch, loci = coordinates, extMag = extMag)

  logger.debug(f'B dict: {B_dict}')

  # return B local
  B = B_dict['Blocal'][0]

  logger.debug(f'B (return): {B}')

  return B


def get_zenith(pitch: float, roll: float, yaw: float) -> float:
  '''
  Get AMS zenith angle from ISS flight angles (i.e. pitch, roll, yaw).

  Parameters:
  - pitch, roll, yaw: flight angles (units: deg)

  Returns:
  zenith angle (units: deg)
  '''

  logger = _LOGGER.getChild('get_zenith')

  ## args

  logger.debug(f'args: pitch: {pitch}, roll: {roll}, yaw: {yaw}')

  # convert angles to radians
  pitch = np.deg2rad(pitch)
  roll = np.deg2rad(roll)
  yaw = np.deg2rad(yaw)

  # get AMS view direction
  ams = R.from_rotvec(_AMS_ROTVEC).apply([0,0,-1])

  # apply pitch, roll, yaw

  R_pry = R.from_rotvec([0,0,0]) # null rotation (i.e. identity matrix)

  # the order of the product is relevant
  for angle, axis in zip([yaw, pitch, roll], [[0,0,1], [0,1,0], [1,0,0]]):
    R_pry *= R.from_rotvec(angle * np.array(axis))

  ams = R_pry.apply(ams)

  logger.debug(f'AMS pointing direction: {ams}')

  # zenith is the angle between the axis that points to the sky ([0,0,-1]) and
  # AMS view direction
  zenith = np.arccos(np.dot(ams, [0,0,-1]) / linalg.norm(ams))
  zenith = np.rad2deg(zenith)

  logger.debug(f'zenith (return): {zenith}')

  return zenith
