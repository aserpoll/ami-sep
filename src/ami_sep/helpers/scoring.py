'''
SEP monitoring.
'''

from logging import getLogger
from typing import Union, Optional
import pandas as pd
import numpy as np
import joblib as jb
from scipy.stats import rv_discrete, rv_continuous, norm, poisson
from . import _LOCAL_FIELDS


_LOGGER = getLogger().getChild(__name__)


# feature to bin
_BINNED = _LOCAL_FIELDS['L']

# name of the column to inject in buffered dataframes to store bins
_BINNED_BINS = f'{_BINNED}_bin'


_LOGGER.debug(f'binned feature: {_BINNED}')
_LOGGER.debug(f'bins column to inject: {_BINNED}')


class Buffer:
  '''
  Fixed time-length buffer.
  '''

  _logger = _LOGGER.getChild('classBuffer')

  _length: pd.Timedelta = None
  _end: pd.Timestamp = None
  _buffer: pd.DataFrame = None


  def __init__(self, length: pd.Timedelta, end: Optional[pd.Timestamp] = None):

    logger = self._logger.getChild('__init__')

    logger.debug(f'args: length: {length}, end: {end}')

    # check length
    if not isinstance(length, pd.Timedelta):
      raise TypeError('length must be a Pandas timedelta')

    # default end
    if end is None:
      end = pd.Timestamp.now()
      logger.debug(f'default end: {end}')

    # parse end
    end = pd.Timestamp(end)
    logger.debug(f'parsed end: {end}')

    # localize end timestamp
    if end.tz is None:
      end = end.tz_localize('utc')

    self._length = length
    self._end = end
    self._buffer = pd.DataFrame()

    logger.debug(
      f'self: length: {self._length}, end: {self._end}, '
      f'buffer shape: {self._buffer.shape}'
    )


  def get_columns(self) -> list:
    return self._buffer.columns.tolist()


  def get_copy(self) -> pd.DataFrame:
    return self._buffer.copy()


  def get_start(self) -> pd.Timestamp:
    return self._end - self._length


  def get_end(self) -> pd.Timestamp:
    return self._end


  def _outdate(self) -> pd.DataFrame:
    '''
    Discard outdated records from buffer.

    Returns:
    outdated records
    '''

    logger = self._logger.getChild('_outdate')

    logger.debug('outdating records')

    start = self.get_start()
    end = self.get_end()

    logger.debug(f'time range: {start} -- {end}')

    t = self._buffer.index.to_series()
    mask = (start <= t) & (t <= end)

    outdated = self._buffer.loc[mask == False]
    self._buffer = self._buffer.loc[mask]

    logger.debug(f'updated buffer shape: {self._buffer.shape}')
    logger.debug(f'outdated records shape (return): {outdated.shape}')

    return outdated


  def move(self, t: pd.Timestamp) -> pd.DataFrame:
    '''
    Move the end of buffer.

    Parameters:
    - t: new end

    Returns:
    outdated records
    '''

    logger = getLogger(f'Buffer.move')

    logger.debug(f'args: t: {t}')

    # parse timestamp
    t = pd.Timestamp(t)

    # move end
    logger.debug(f'previous end: {self._end}')
    self._end = t
    logger.debug(f'new end: {self._end}')

    outdated = self._outdate()
    logger.debug(f'outdated shape (return): {outdated.shape}')

    return outdated


  def append(self, df: pd.DataFrame) -> pd.DataFrame:
    '''
    Append new data to buffer.

    New buffer end will be updated to `max(df.index.max, buffer.end)`.

    Dataframes added at different times must contain the same columns.

    Parameters:
    - df: input dataframe; it must be time-indexed

    Returns:
    outdated records
    '''

    logger = self._logger.getChild('append')

    logger.debug(f'args: input shape: {df.shape}')

    # check dataframe index
    if not isinstance(df.index, pd.DatetimeIndex):
      raise ValueError('input dataframe must be time indexed')

    # check dataframe columns
    if self._buffer.shape[1] != 0:

      input_cols = df.columns.tolist()
      cols = self.get_columns()

      input_cols.sort()
      cols.sort()

      if input_cols != cols:
        raise ValueError(
          f'input dataframe must contain the columns {self.get_columns()}'
        )

    # void input dataframe
    if df.shape[0] == 0:
      logger.debug('input dataframe is void, returning input dataframe')
      return df

    # buffer time range
    start = self.get_start()
    end = self.get_end()
    logger.debug(f'buffer: start: {start}, end: {end}')

    # input time range
    df_start = df.index.to_series().min()
    df_end = df.index.to_series().max()
    logger.debug(f'input: start: {df_start}, end: {df_end}')

    # a completely outdated input is given
    if df_end < start:
      logger.debug('input dataframe contains only out of date records')
      return df

    outdated = pd.DataFrame()

    # move buffer's end to input's and store buffer's outdated records
    if df_end > self._end:

      logger.debug('moving buffer\'s end to input dataframe\'s one')
      outdated = pd.concat([outdated, self.move(df_end)])

      # update buffer's time range after move
      start = self.get_start()
      end = self.get_end()

    # actually append input
    self._buffer = pd.concat([self._buffer, df])

    # check if input has outdated records
    if df_start < start:
      outdated = pd.concat([outdated, self._outdate()])

    logger.debug(f'outdated shape (return): {outdated.shape}')

    return outdated


class Analyzer:
  f'''
  Analyze data for SEP monitoring.

  The detection is based on scores defined using p-values and distributions
  built from bufferized past data.
  Distributions are generated within a single {_BINNED} bin.
  '''

  _logger = _LOGGER.getChild('classAnalyzer')

  _bins_res: float = None
  _buffer: Buffer = None

  _count: pd.DataFrame = None
  _seconds: pd.DataFrame = None
  _mean: pd.DataFrame = None
  _rms: pd.DataFrame = None


  def __init__(
    self, bins_res: float, length: pd.Timedelta,
    end: Optional[pd.Timestamp] = None
  ):
    f'''
    Parameters:
    - bins_res: {_BINNED} bins resolution
    - length: time length of the internal buffer
    - end: initial end of internal buffer
    '''

    logger = self._logger.getChild('__init__')

    logger.debug(f'args: bins_res: {bins_res}, length: {length}, end: {end}')

    self._bins_res = bins_res
    self._buffer = Buffer(length, end)

    self._count = pd.DataFrame()
    self._seconds = pd.DataFrame()
    self._mean = pd.DataFrame()
    self._rms = pd.DataFrame()


  def get_buffer_columns(self) -> list:
    return self._buffer.get_columns()


  @staticmethod
  def compute_score(x: float, mu: float, sigma: float, dist: str) -> float:
    '''
    Parameters:
    - x: measure
    - mu: expected value
    - sigma: `norm` distribution sigma; this argument is ignored if the
      the selected distribution is a poisson
    - dist: scoring distribution name (`poisson` or `norm`)

    Returns:
    score computed as `1 - cdf(x)` (i.e. right-sided p-value)
    '''

    logger = _LOGGER.getChild('classAnalyzer_staticmethod.compute_score')

    if dist == "norm":
      s = norm.cdf(x, mu, sigma)
    elif dist == "poisson":
      s = poisson.cdf(x, mu)
    else:
      raise ValueError(
        'only poisson and norm distributions are supported ' + \
          f'({dist} has been passed)'
      )

    return 1 - s


  def get_buffer_copy(self) -> pd.DataFrame:
    return self._buffer.get_copy()


  def get_buffer_start(self) -> pd.Timestamp:
    return self._buffer.get_start()


  def get_buffer_end(self) -> pd.Timestamp:
    return self._buffer.get_end()


  def get_moments_copy(self) -> tuple:
    return self._mean.copy(), self._rms.copy()


  def _get_bin_center(self, x):
    return (x // self._bins_res + 0.5) * self._bins_res


  def _update_moments(self):
    '''
    Update moments of internal buffer.
    '''

    logger = self._logger.getChild('_update_moments')

    logger.debug('updating moments')

    # get buffer copy
    df = self.get_buffer_copy()

    # group data by bins
    df_group = df.drop(columns = [_BINNED]).groupby(_BINNED_BINS)

    # compute moments

    def compute_seconds(x: pd.Series) -> float:
      '''
      Compute seconds covered by a series.

      Parameters:
      - x: time-indexed series

      Returns:
      seconds covered or nan when there are less than 2 valid timestamps
      '''

      # check time index
      if not isinstance(x.index, pd.DatetimeIndex):
        raise ValueError('input series must be time indexed')

      x = x.dropna().index.to_series()

      if x.shape[0] < 2:
        return np.nan

      return x.sort_values().iloc[[0,-1]].diff().iloc[-1].total_seconds()

    self._count = df_group.count()
    self._seconds = df_group.agg(compute_seconds)
    self._mean = df_group.mean()
    self._rms = df_group.std()

    logger.debug(
      f'self: count shape: {self._count.shape}, '
      f'seconds shape: {self._seconds.shape}, mean shape: {self._mean.shape}, '
      f'rms shape: {self._rms.shape}'
    )


  def _concat_smooth_moments(self, df: pd.DataFrame) -> pd.DataFrame:
    '''
    Concat smoothed moments to dataframe.
    Input dataframe must contain mean, rms, count and be multi-indexed in its
    columns.

    Smoothed moments are calculated using the moments of the closest two bin
    centers and the distances of the measure from them as weights for the
    average.

    Parameters:
    - df: multi-indexed dataframe containing mean, rms, count.

    Return:
    dataframe containing the input frame and the smoothed moments.
    '''

    logger = self._logger.getChild('_concat_smooth_moments')

    logger.debug(f'args: dataframe shape: {df.shape}')

    # modify a copy, not the original dataframe
    df = df.copy()

    def get_prev_next(x):
      '''
      Get copies shifted of one bin; first copy align values of previous bin to
      the current one, the second the ones of the next bin.
      '''

      r = []

      for shift in [self._bins_res, - self._bins_res]:
        r.append(x.copy())
        r[-1].index = r[-1].index.to_series() + shift
        r[-1] = r[-1].loc[r[-1].index.to_series().isin(x.index.to_series())]

      return tuple(r)

    # distance between actual measure and bin center
    delta = (df.loc[:, (_BINNED, 'value')] - df.index.to_series())

    # weight for other (prev or next) bin (current bin weight is 1-w)
    w = delta.abs() / self._bins_res

    # sign of distance is used to select prev or next bin
    delta_sign = delta.apply(np.sign)

    for key in ['mean', 'rms', 'count']:

      x = df.loc[:, (slice(None), key)].droplevel(-1, axis = 1)
      x_prev, x_next = get_prev_next(x)

      x_smooth = x.mul(1 - w, axis = 0) + (
        x_prev.mul(delta_sign == -1, axis = 0)
        + x_next.mul(delta_sign == 1, axis = 0)
      ).mul(w, axis = 0)

      # fill missing values with original ones
      x_smooth = x_smooth.where(x_smooth.isna() == False, x)

      x_smooth.columns = pd.MultiIndex.from_product(
        [x_smooth.columns.tolist(), [f'{key}_smooth']]
      )

      df = pd.concat([df, x_smooth], axis = 1)

    logger.debug(f'dataframe shape (return): {df.shape}')

    return df


  def score(
    self, df: pd.DataFrame, dist: Optional[dict] = None, n_jobs: int = -1
  ) -> pd.DataFrame:
    f'''
    Score data using `Analyzer.compute_score`.

    The end of internal buffer, used to generate past data distributions, will
    be aligned to the oldest timestamp of the input dataframe before scoring.

    The returned dataframe will contain for each record the original {_BINNED}
    value, the center of the evaluated bin and for each scored feature the
    original measurement, the count of records used for the distribution, the
    mean and the rms of the distribution and the computed score.
    Columns will be multi-indexed as `(feature, value_type)`.

    Parameters:
    - df: input dataframe; it must be time-indexed and contain {_BINNED} and
      features
    - dist: scoring distributions (i.e. `norm` or `poisson`) passed in a dict
      (feature name: distribution); missing features will be assumed as normal
      distributed, if the arg is `None`, all features will be assumed as normal
      distributed
    - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
      reference for more info)

    Returns:
    dataframe
    '''

    logger = self._logger.getChild('score')

    logger.debug(f'args: dataframe shape: {df.shape}, dist: {dist}')

    # check dataframe columns
    if _BINNED not in df.columns.tolist():
      raise ValueError(f'input dataframe must contain {_BINNED} values')

    # default dist
    if dist is None:
      dist = {}

    # move buffer forward
    outdated = self._buffer.move(df.index.to_series().min())

    # update moments only if the buffer has changed
    if outdated.shape[0] != 0:
      self._update_moments()

    # features to score
    features = df.columns.tolist()
    features.remove(_BINNED)

    logger.debug(f'features: {features}')

    # save original index name for later use

    original_idx = df.index.name

    if original_idx is None:
      original_idx = 'index'

    logger.debug(f'original index: {original_idx}')

    # reset index without dropping the original one
    df = df.reset_index(drop = False)

    # index by bins; now df and stored moments can be concatenated
    df.index = self._get_bin_center(df.loc[:, _BINNED])

    # concat moments

    df = {
      'value': df, 'count': self._count, 'seconds': self._seconds,
      'mean': self._mean, 'rms': self._rms
    }

    keys = list(df.keys())
    df = list(df.values())

    df = pd.concat(df, keys = keys, axis = 1)

    for key in keys:
      if key not in df.columns.get_level_values(0).tolist():
        for feature in features:
          logger.debug(f'key {key} is missing, filling with NaNs')
          df.insert(0, (key, feature), [np.nan] * df.shape[0])

    # (value_type, feature) --> (feature, value_type)
    df = df.swaplevel(axis = 1)

    # smooth moments
    df = self._concat_smooth_moments(df)

    # drop lines that are not from an input record
    df = df.dropna(subset = [(original_idx, 'value')])

    # compute scores and smooth scores
    for suff in ['', '_smooth']:

      scores = jb.Parallel(n_jobs = n_jobs)(
        jb.delayed(
          lambda x, dist: \
            Analyzer.compute_score(*x.loc[['value', f'mean{suff}', f'rms{suff}']], dist) \
            if pd.Series(['value', f'mean{suff}', f'rms{suff}']).isin(x.index.tolist()).all() \
            and not x.loc[['value', f'mean{suff}', f'rms{suff}']].isna().any() \
            else np.nan
        )(
          df.iloc[i].loc[feature], dist.get(feature, "norm")
        )
        for feature in features
        for i in range(df.shape[0])
      )

      # concat scores
      scores = pd.DataFrame(
        np.reshape(scores, (df.shape[0], -1)), index = df.index,
        columns = pd.MultiIndex.from_product([features, [f'score{suff}']])
      )
      df = pd.concat([df, scores], axis = 1)

    # concat bins
    bins = df.index.to_series()
    bins.name = (_BINNED, 'bin')
    df = pd.concat([df, bins], axis = 1)

    # restore the original index
    df = df.set_index((original_idx, 'value'))
    df.index.name = original_idx

    # name columns levels
    df.columns.names = ('feature', 'value_type')

    df = df.sort_index(axis = 1)

    logger.debug(f'dataframe shape (return): {df.shape}')

    return df


  def buffer(self, df: pd.DataFrame) -> None:
    f'''
    Append data to the internal buffer for scoring distributions.

    Parameters:
    - df: input dataframe; it must be time-indexed and contain {_BINNED} and
      features
    '''

    logger = self._logger.getChild('buffer')

    logger.debug(f'args: dataframe shape: {df.shape}')

    # check dataframe shape
    if df.shape[0] == 0:
      logger.debug('input dataframe is void, returning')
      return

    # check dataframe columns
    if _BINNED not in df.columns.tolist():
      raise ValueError(f'input dataframe must contain {_BINNED} values')

    # inject bins
    df.insert(0, _BINNED_BINS, self._get_bin_center(df.loc[:, _BINNED]))

    # append
    outdated = self._buffer.append(df)

    # update moments only if the appended dataframe is not rejected as outdated
    if outdated.shape[0] != df.shape[0] \
      or (outdated.index.sort_values() != df.index.sort_values()).any():

      self._update_moments()
