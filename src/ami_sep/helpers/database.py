'''
Querying and writing operations on InfluxDB instances containing AMS data.
'''


import json
from logging import getLogger
from urllib.parse import urlencode
from warnings import warn
from typing import Optional
import pandas as pd
import numpy as np
import joblib as jb
from tqdm import trange
from . import _LOCAL_FIELDS, mySession, _get_progress_kwargs, compact_records


# module logger
_LOGGER = getLogger().getChild(__name__)


# place-holder for inf when writing to influxdb
_INFLUX_INF = 1e100

# default value for remote InfluxDB url
_DEFAULT_REMOTE = 'https://dbod-ams-ami.cern.ch:8081'

# AMS InfluxDB API parameters
_API = {
  'query_endpoint': 'query',
  'write_endpoint': 'write',
  'influxdb_version': (1,8)
}

# AMS database name
_DB = 'AMS_ISS'

# AMS database measurements names
_MEAS = {
  'gps': 'GPS_Info_v1',
  'flight': 'ISSLive',
  'flight_backup': 'ISS_LVLH',
  'trig': 'JLV1'
}

# AMS measurements fields & tags names and epoch field name
_FIELDS = {

  'epoch': 'time',

  'gps': {
    'x': 'X',
    'y': 'Y',
    'z': 'Z',
    'latitude': 'latitude',
    'longitude': 'longitude',
    'altitude': 'altitude',
    'velocity_x': 'VX',
    'velocity_y': 'VY',
    'velocity_z': 'VZ'
  },

  'flight': {
    'pitch': 'curr_pitch',
    'roll': 'curr_roll',
    'yaw': 'curr_yaw'
  },

  'flight_backup': {
    'pitch': 'pitch',
    'roll': 'roll',
    'yaw': 'yaw'
  },

  'trig': {
    'livetime': 'livetime_0',
    'LV1': 'LV1',
    'FT': 'FT',
    'FTZ': 'FTZ',
    'FTC': 'FTC',
    'FTE': 'FTE',
    'subLV1_0': 'subLV1_0',
    'subLV1_1': 'subLV1_1',
    'subLV1_2': 'subLV1_2',
    'subLV1_3': 'subLV1_3',
    'subLV1_4': 'subLV1_4',
    'subLV1_5': 'subLV1_5',
    'subLV1_6': 'subLV1_6'
  }
}

# filters for measure download (appended to query's WHERE statement)
_FILTERS = {}

# acquisition rate of flight angles
_FLIGHT_BACKUP_RESAMPLE = 60 # s

# warning level for rms/mean ratio for newly aggregated records
_WARN_AGG_DUPLICATES = 0.01

# data points to write on a single API call to InfluxDB
_WRITE_CHUNK_SIZE = 5000


# log module's global variables
_LOGGER.debug(f'place-holder for inf values in InfluxDB: {_INFLUX_INF}')
_LOGGER.debug(f'default value for remote: {_DEFAULT_REMOTE}')
_LOGGER.debug(f'API parameters: \n{json.dumps(_API, indent = 2)}')
_LOGGER.debug(f'AMS database name: {_DB}')
_LOGGER.debug(f'AMS measurements names: \n{json.dumps(_MEAS, indent = 2)}')
_LOGGER.debug(
  f'AMS fields and tags names: \n{json.dumps(_FIELDS, indent = 2)}'
)
_LOGGER.debug(f'AMS filters for queries: \n{json.dumps(_FILTERS, indent = 2)}')
_LOGGER.debug(
  f'resampling rate for old ISS flight angles data: {_FLIGHT_BACKUP_RESAMPLE}'
)
_LOGGER.debug(
  'warning level for rms/mean ratio for newly aggregated records: '
  f'{_WARN_AGG_DUPLICATES}'
)
_LOGGER.debug(f'write chunk size: {_WRITE_CHUNK_SIZE}')


def _parse_response(r: str) -> Optional[pd.DataFrame]:
  '''
  Parse json response (as a string) of a `SELECT` query.

  Parameters:
  - r: json response as a string

  Returns:
  dataframe or `None` if no results have been found
  '''

  logger = _LOGGER.getChild('_parse_response')

  r = json.loads(r)

  df = r['results'][0]

  try:
    df = df['series'][0]
  except KeyError:
    logger.debug('no results, returning None')
    return None

  df = pd.DataFrame(df['values'], columns = df['columns'])

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def query(
  columns: list, table: str, where: Optional[str] = None,
  remote: str = _DEFAULT_REMOTE, username: Optional[str] = None,
  password: Optional[str] = None
) -> Optional[pd.DataFrame]:
  '''
  Query AMS InfluxDB.

  Parameters:
  - columns: columns to select (fields and tags)
  - table: table to use
  - where: WHERE statement clauses
  - remote: remote database url
  - username: API username
  - password: API password

  Returns:
  dataframe or `None` if no results
  '''

  logger = _LOGGER.getChild('query')

  ## args

  logger.debug(
    f'args: columns: {columns}, table: {table}, where: {where}, '
    f'remote: {remote}, username: {username}, password: {password}'
  )

  ## query

  q = f'SELECT {",".join(columns)} FROM {table} '

  if where is not None:
    q += f'WHERE {where}'

  logger.debug(f'query string: {q}')

  url = f"{remote}/{_API['query_endpoint']}?"

  for key, value in {'u': username, 'p': password}.items():
    if value is not None:
      url += f'{key}={value}&'

  url += urlencode({'q': q})
  
  logger.debug(f'query url: {url}')

  with mySession() as session:
    r = session.get(url)

  ## check response

  logger.debug(f'query response: status code: {r.status_code}')

  if r.status_code != 200:
    raise RuntimeError(f'query to AMS data failed: {r.status_code}: {r.text}')

  ## parse response

  df = _parse_response(r.text)

  if df is None:
    return None

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def write(
  db: str, meas: str, df: pd.DataFrame,
  timestamps: str = _LOCAL_FIELDS['epoch'], remote: str = _DEFAULT_REMOTE,
  username: Optional[str] = None, password: Optional[str] = None,
  n_jobs: int = -1, progress_kwargs: Optional[dict] = None
):
  '''
  Write a dataframe to an InfluxDB.

  Parameters:
  - db: database name
  - meas: measurement name
  - df: input dataframe; index levels are treated as tags (except for the level
    passed as `timestamps`) and columns as fields 
  - timestamps: name of the dataframe's index level containing timestamps
  - remote: remote database url
  - username: API username
  - password: API password
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more information)
  - progress_kwargs: kwargs for progress bar (i.e. `tqdm.tqdm` object)
  '''

  logger = _LOGGER.getChild('write')

  ## args

  logger.debug(
    f'args: db: {db}, meas: {meas}, df shape: {df.shape}, '
    f'timestamps: {timestamps}, remote: {remote}, username: {username}, '
    f'password: {password}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  ## write

  # url

  url = f"{remote}/{_API['write_endpoint']}?"

  for key, value in {'u': username, 'p': password}.items():
    if value is not None:
      url += f'{key}={value}&'

  url += f'db={db}'

  logger.debug(f'url: {url}')

  # data

  def write_line(i: int) -> str:
    '''
    Write one line of the dataframe as an InfluxDB writing line.

    Parameters:
    - i: line lumber of the dataframe

    Returns:
    string
    '''

    if df.iloc[i].isna().all():
      return '# null row' # it is a comment: InfluxDB will ignore it

    s = f'{meas},'

    for tag in df.index.names:

      if tag == timestamps:
        continue

      value = df.index.get_level_values(tag)[i]

      if isinstance(value, float):
        if np.isnan(value):
          continue
        if np.isinf(value):
          value = np.sign(value) * _INFLUX_INF

      s += f'{tag}={value},'

    s = s[:-1] + ' ' # replace trailing comma with a space

    for field in df.columns.tolist():

      value = df.iloc[i].loc[field]

      if isinstance(value, float):
        if np.isnan(value):
          continue
        if np.isinf(value):
          value = np.sign(value) * _INFLUX_INF

      s += f'{field}={value},'

    s = s[:-1] + ' ' # replace trailing comma with a space

    s += f'{df.index.get_level_values(timestamps)[i].value}'

    return s

  for chunk_start in trange(
    0, df.shape[0], _WRITE_CHUNK_SIZE, **_get_progress_kwargs(progress_kwargs)
  ):

    chunk_end = chunk_start + _WRITE_CHUNK_SIZE

    if chunk_end > df.shape[0]:
      chunk_end = df.shape[0]

    data = jb.Parallel(n_jobs = n_jobs)(
      jb.delayed(write_line)(i) for i in range(chunk_start, chunk_end)
    )

    data = ' \n'.join(data)

    with mySession() as session:
      r = session.post(url, data = data.encode('utf-8'))

    ## check response

    logger.debug(f'write response: status code: {r.status_code}')

    if r.status_code // 100 != 2:
      raise RuntimeError(f'write request failed: {r.status_code}: {r.text}')


def _rename_columns(
  df: pd.DataFrame, meas_key: str, fields_keys: list
) -> pd.DataFrame:
  '''
  Rename AMS data columns names to local ones.

  Parameters:
  - df: input dataframe
  - meas_key: key of measurement in `_MEAS` dict
  - fields_keys: list of keys of the fields in `_FIELDS[meas_key]` dict

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('_rename_columns')

  logger.debug(
    f'args: dataframe columns: {df.columns.tolist()}, meas_key: {meas_key}, '
    f'fields_key: {fields_keys}'
  )

  df = df.rename(columns = {_FIELDS['epoch']: _LOCAL_FIELDS['epoch']})
  df = df.rename(columns = {
    _FIELDS[meas_key][key]: _LOCAL_FIELDS[key] for key in fields_keys
  })

  logger.debug(f'dataframe columns (return): {df.columns.tolist()}')

  return df


def _parse_timestamps(df: pd.DataFrame) -> pd.DataFrame:
  '''
  Parse dataframe index to timestamps.

  Parameters:
  - df: input dataframe

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('_parse_timestamps')

  logger.debug(f'args: dataframe index dtype: {df.index.to_series().dtype}')

  df.index = pd.to_datetime(df.index.to_series(), utc = True)

  logger.debug(f'dataframe index dtype (return): {df.index.to_series().dtype}')

  return df


def _agg_duplicates(df: pd.DataFrame) -> pd.DataFrame:
  '''
  Aggregate records with the same index.

  Parameters:
  - df: input dataframe

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('_agg_duplicates')

  logger.debug(
    f'args: dataframe: shape: {df.shape}, duplicates: '
    f'{df.shape[0] - df.index.to_series().nunique()}'
  )

  df = df.groupby(df.index.to_series())

  # warn about duplicates that seem to be different and log them

  non_similar = (df.std() / df.mean()).loc[
    lambda x: (x > _WARN_AGG_DUPLICATES).any(axis = 1)
  ]

  if non_similar.shape[0] > 0:

    desc = non_similar.reset_index().describe(datetime_is_numeric = True)

    warn(
      'some newly aggregated duplicate records have a rms/mean ratio > '
      f'{_WARN_AGG_DUPLICATES}'
    )

    logger.info(
      'Description of newly aggregated duplicate records with a high rms/mean '
      f'ratio (warning level set: {_WARN_AGG_DUPLICATES}): \n{desc.to_string()}'
    )

  # actually aggregate duplicates
  df = df.mean()

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def _preprocess(
  df: pd.DataFrame, meas_key: str, fields_keys: list
) -> pd.DataFrame:
  '''
  Preliminary preprocessing of AMS data: rename columns, parse timestamps,
  aggregate duplicates and compact records.

  Parameters:
  - df: input dataframe
  - meas_key: key of measurement in `_MEAS` dict
  - fields_keys: list of keys of the fields in `_FIELDS[meas_key]` dict

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('_preprocess')

  logger.debug(
    f'args: dataframe shape: {df.shape}, meas_key: {meas_key}, '
    f'fields_keys: {fields_keys}'
  )

  df = _rename_columns(df, meas_key, fields_keys)
  df = df.set_index(_LOCAL_FIELDS['epoch'])

  if df.shape[0] > 0:
    df = _parse_timestamps(df)
    df = _agg_duplicates(df)
    df = compact_records(df)
  else:
    df.index = pd.DatetimeIndex([], name = df.index.name)

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def _get(
  start: pd.Timestamp, end: pd.Timestamp, meas_key: str,
  fields_keys: Optional[list] = None, remote: str = _DEFAULT_REMOTE,
  username: Optional[str] = None, password: Optional[str] = None
) -> pd.DataFrame:
  '''
  Query AMS data and return the preprocessed dataframe.

  Parameters:
  - start: start date expressed as a timestamp parsable by `pandas.Timestamp`
  - end: end date expressed as a timestamp parsable by `pandas.Timestamp`
  - meas_key: key of measurement in `_MEAS` dict
  - fields_keys: list of keys of the fields in `_FIELDS[meas_key]` dict to be
    queried; if `None` all fields of given measurement are used
  - remote: remote database url
  - username: API username
  - password: API password

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('_get')

  ## args

  logger.debug(
    f'args: start: {start}, end: {end}, meas_key: {meas_key}, '
    f'fields_keys: {fields_keys}, remote: {remote}, username: {username}, '
    f'password: {password}'
  )

  # check meas
  if meas_key not in _MEAS:
    raise ValueError(f'{meas_key} is not an AMS measurement')

  # default fields
  if fields_keys is None:
    fields_keys = list(_FIELDS[meas_key].keys())
    logger.debug(f'default fields keys: {fields_keys}')

  # check fields
  for key in fields_keys:
    if key not in _FIELDS[meas_key]:
      raise ValueError(
        f'fields {fields_keys} are not valid for AMS measurement {meas_key}'
      )

  # parse timestamps
  start = pd.Timestamp(start).value
  end = pd.Timestamp(end).value

  ## query

  columns = [_FIELDS[meas_key][key] for key in fields_keys]
  table = f'{_DB}..{_MEAS[meas_key]}'
  where = f'time >= {start} AND time <= {end} '

  if meas_key in _FILTERS:
    where += f'AND ({_FILTERS[meas_key]}) '

  df = query(columns, table, where, remote, username, password)

  if df is None:
    df = pd.DataFrame(columns = [_FIELDS['epoch']] + columns)
    logger.debug('no results, using a dataframe without records')

  ## preprocess

  df = _preprocess(df, meas_key, fields_keys)

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def get_car_positions(
  start: pd.Timestamp, end: pd.Timestamp, remote: str = _DEFAULT_REMOTE,
  username: Optional[str] = None, password: Optional[str] = None
) -> pd.DataFrame:
  '''
  Get AMS cartesian positions (GEO system; units: m).

  Parameters:
  - start: start date expressed as a timestamp parsable by `pandas.Timestamp`
  - end: end date expressed as a timestamp parsable by `pandas.Timestamp`
  - remote: remote database url
  - username: API username
  - password: API password

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('get_car_positions')

  ## args

  logger.debug(
    f'args: start: {start}, end: {end}, remote: {remote}, '
    f'username: {username}, password: {password}'
  )

  ## get

  df = _get(start, end, 'gps', list('xyz'), remote, username, password)

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def get_sph_positions(
  start: pd.Timestamp, end: pd.Timestamp, remote: str = _DEFAULT_REMOTE,
  username: Optional[str] = None, password: Optional[str] = None
) -> pd.DataFrame:
  '''
  Get AMS spherical positions (WGS84 system; units: [deg, deg, m]).

  Parameters:
  - start: start date expressed as a timestamp parsable by `pandas.Timestamp`
  - end: end date expressed as a timestamp parsable by `pandas.Timestamp`
  - remote: remote database url
  - username: API username
  - password: API password

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('get_sph_positions')

  ## args

  logger.debug(
    f'args: start: {start}, end: {end}, remote: {remote}, '
    f'username: {username}, password: {password}'
  )

  ## get

  df = _get(
    start, end, 'gps', ['latitude', 'longitude', 'altitude'], remote, username,
    password
  )

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def get_velocities(
  start: pd.Timestamp, end: pd.Timestamp, remote: str = _DEFAULT_REMOTE,
  username: Optional[str] = None, password: Optional[str] = None
) -> pd.DataFrame:
  '''
  Get AMS cartesian velocities (GEO system; units: m/s).

  Parameters:
  - start: start date expressed as a timestamp parsable by `pandas.Timestamp`
  - end: end date expressed as a timestamp parsable by `pandas.Timestamp`
  - remote: remote database url
  - username: API username
  - password: API password

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('get_velocities')

  ## args

  logger.debug(
    f'args: start: {start}, end: {end}, remote: {remote}, '
    f'username: {username}, password: {password}'
  )

  ## get

  df = _get(
    start, end, 'gps', ['velocity_x', 'velocity_y', 'velocity_z'], remote,
    username, password
  )

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def get_orientations(
  start: pd.Timestamp, end: pd.Timestamp, remote: str = _DEFAULT_REMOTE,
  username: Optional[str] = None, password: Optional[str] = None
) -> pd.DataFrame:
  '''
  Get ISS flight angles (i.e. pitch, roll, yaw).

  Parameters:
  - start: start date expressed as a timestamp parsable by `pandas.Timestamp`
  - end: end date expressed as a timestamp parsable by `pandas.Timestamp`
  - remote: remote database url
  - username: API username
  - password: API password

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('get_orientations')

  ## args

  logger.debug(
    f'args: start: {start}, end: {end}, remote: {remote}, '
    f'username: {username}, password: {password}'
  )

  ## get

  df = _get(
    start, end, 'flight', remote = remote, username = username,
    password = password
  )

  if df.shape[0] == 0:
    logger.debug('no results, trying backup fields names')
    df = _get(
      start, end, 'flight_backup', remote = remote, username = username,
      password = password
    )

    # flight angles are currently generated every 1 minute
    # old values were generated every one second
    # old data are resampled to meet the same frequency of current data

    df = df.resample(pd.Timedelta(seconds = _FLIGHT_BACKUP_RESAMPLE)).apply(
      lambda x: x.iloc[0] if x.shape[0] > 0 else None
    )

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def get_features(
  start: pd.Timestamp, end: pd.Timestamp, remote: str = _DEFAULT_REMOTE,
  username: Optional[str] = None, password: Optional[str] = None
) -> pd.DataFrame:
  '''
  Get AMS features for SEP monitoring.

  Parameters:
  - start: start date expressed as a timestamp parsable by `pandas.Timestamp`
  - end: end date expressed as a timestamp parsable by `pandas.Timestamp`
  - remote: remote database url
  - username: API username
  - password: API password

  Returns:
  dataframe
  '''

  logger = _LOGGER.getChild('get_features')

  ## args

  logger.debug(
    f'start: {start}, end: {end}, remote: {remote}, '
    f'username: {username}, password: {password}'
  )

  ## get

  df = _get(
    start, end, 'trig', remote = remote, username = username,
    password = password
  )

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df
