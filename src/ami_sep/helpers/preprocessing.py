'''
AMS data preprocessing for SEP scoring.
'''

from logging import getLogger
from typing import Optional, Callable
from functools import partial
import pandas as pd
import numpy as np
from tqdm import tqdm
from . import \
  _LOCAL_FIELDS, _RATES_SUFFIXES, _get_progress_kwargs, parallel_apply, \
  compact_records
from .physics import get_earth_radii, get_B, get_L, get_zenith


_LOGGER = getLogger().getChild(__name__)


# default rates to be divided by livetime
_DEFAULT_CORR_RATES = [
  _LOCAL_FIELDS[key]
  for key in [
    'LV1', 'FT', 'FTZ', 'FTC', 'FTE', 'subLV1_0', 'subLV1_1', 'subLV1_2',
    'subLV1_3', 'subLV1_4', 'subLV1_5'
  ]
]

# default ratios between rates
_DEFAULT_RATES_RATIOS = {}


# log global variables
_LOGGER.debug(f'default corrected rates: {_DEFAULT_CORR_RATES}')
_LOGGER.debug(f'default rates ratios: {_DEFAULT_RATES_RATIOS}')


def _get_data_age(
  df: pd.DataFrame, subset: Optional[list] = None, n_jobs: int = -1,
  progress_kwargs: Optional[dict] = None
) -> pd.Series:
  '''
  Get delta time between a record and the last observation in the past.

  Parameters:
  - df: input dataframe
  - subset: subset of observations features; if `None` all columns are
    considered
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Returns:
  series of deltas expressed in total seconds
  '''

  logger = _LOGGER.getChild('_get_data_age')

  logger.debug(
    f'args: dataframe shape: {df.shape}, subset: {subset}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  if subset is None:
    subset = df.columns.tolist()
    logger.debug(f'using subset {subset}')

  # timestamps of valid observations
  t_obs = df.loc[:, subset].dropna().index.to_series()
  logger.debug(
    f'shape of series of timestamps of valid observations: {t_obs.shape}'
  )

  # timestamps of records with null values
  t_null = df.loc[:, subset].index.to_series().drop(t_obs)
  logger.debug(
    f'shape of series of timestamps of records with null values: {t_null.shape}'
  )

  # timestamps of the last valid observation
  t_last_obs = pd.concat([
    t_obs, pd.Series([pd.NaT] * t_null.shape[0], index = t_null.index)
  ])
  t_last_obs = t_last_obs.sort_index()
  t_last_obs = t_last_obs.ffill()
  logger.debug(
    'shape of series of timestamps of last valid observation: '
    f'{t_last_obs.shape}'
  )

  # NaT are always tz-naive, so all timestamps are converted to tz-naive before
  # the subtraction
  delta = parallel_apply(
    (
      t_last_obs.index.to_series().apply(lambda t: t.tz_localize(None)) \
        - t_last_obs.apply(lambda t: t.tz_localize(None))
    ).to_frame(),
    lambda t: t.total_seconds() if isinstance(t, pd.Timedelta) else np.nan,
    n_jobs, progress_kwargs
  )

  logger.debug(
    f'description of series of deltas (return): {delta.describe().to_string()}'
  )

  return delta


def insert_pos_age(
  df: pd.DataFrame, n_jobs: int = -1, progress_kwargs: Optional[None] = None
) -> pd.Series:
  '''
  Insert delta times between a record and the last position measured.

  Parameters:
  - df: input dataframe; it must contain cartesian positions
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Returns:
  series of deltas expressed in total seconds
  '''

  logger = _LOGGER.getChild('insert_pos_age')

  logger.debug(
    f'args: dataframe shape: {df.shape}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  x = _get_data_age(
    df, [_LOCAL_FIELDS[key] for key in list('xyz')], n_jobs, progress_kwargs
  )
  x.name = _LOCAL_FIELDS['pos_age']

  df = pd.concat([df, x], axis = 1)

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def insert_orient_age(
  df: pd.DataFrame, n_jobs: int = -1, progress_kwargs: Optional[dict] = None
) -> pd.Series:
  '''
  Insert delta times between a record and the last orientation measured.

  Parameters:
  - df: input dataframe; it must contain orientations
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Returns:
  series of deltas expressed in total seconds
  '''

  logger = _LOGGER.getChild('insert_orient_age')

  logger.debug(
    f'args: dataframe shape: {df.shape}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  x = _get_data_age(
    df, [_LOCAL_FIELDS[key] for key in ['pitch', 'roll', 'yaw']], n_jobs,
    progress_kwargs
  )
  x.name = _LOCAL_FIELDS['orient_age']

  df = pd.concat([df, x], axis = 1)

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def _insert(
  df: pd.DataFrame, func: Callable, ignore_nan: bool = True,
  use_index: bool = False, name: str = 'values', ffill: bool = True,
  n_jobs: int = -1, progress_kwargs: bool = True
) -> pd.DataFrame:
  '''
  Insert a new column of values.

  Parameters:
  - df: input dataframe
  - func: function to apply to dataframe's rows to generate new values
  - ignore_nan: ignore nan values
  - use_index: pass index to func
  - name: name of the new column
  - ffill: forward fill null values
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Return:
  dataframe
  '''

  logger = _LOGGER.getChild('_insert')

  logger.debug(
    f'args: dataframe shape: {df.shape}, func: {func}, '
    f'ignore_nan: {ignore_nan}, use_index: {use_index}, name: {name}, '
    f'ffill: {ffill}, n_jobs: {n_jobs}, progress_kwargs: {progress_kwargs}'
  )

  # do not compute null values
  if ignore_nan:
    x = df.dropna()
  else:
    x = df

  values = parallel_apply(
    x.reset_index() if use_index else x, func = func, n_jobs = n_jobs,
    progress_kwargs = progress_kwargs
  )
  values.name = name

  # restore original index
  if use_index:
    values.index = x.index

  # insert
  df = pd.concat([df, values], axis = 1)

  # fill missing values
  if ffill:
    df.loc[:, name] = df.loc[:, name].fillna(method = 'ffill')

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def insert_B(
  df: pd.DataFrame, n_jobs: int = -1, progress_kwargs: bool = True
) -> pd.DataFrame:
  '''
  Insert geomagnetic field values.

  Parameters:
  - df: input dataframe; it must contain containing cartesian positions
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Return:
  dataframe
  '''

  logger = _LOGGER.getChild('insert_B')

  logger.debug(
    f'args: dataframe shape: {df.shape}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  B = _insert(
    df.loc[:, [_LOCAL_FIELDS[key] for key in list('xyz')]], func = get_B,
    use_index = True, name = _LOCAL_FIELDS['B'], n_jobs = n_jobs,
    progress_kwargs = progress_kwargs
  )
  B = B.loc[:, _LOCAL_FIELDS['B']]

  # insert
  df = pd.concat([df, B], axis = 1)

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def insert_L(
  df: pd.DataFrame, n_jobs: int = -1, progress_kwargs: bool = True
) -> pd.DataFrame:
  '''
  Insert L-values.

  Parameters:
  - df: input dataframe; it must contain cartesian positions
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Return:
  dataframe
  '''

  logger = _LOGGER.getChild('insert_L')

  logger.debug(
    f'args: dataframe shape: {df.shape}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  L = _insert(
    df.loc[:, [_LOCAL_FIELDS[key] for key in list('xyz')]], func = get_L,
    use_index = True, name = _LOCAL_FIELDS['L'], n_jobs = n_jobs,
    progress_kwargs = progress_kwargs
  )
  L = L.loc[:, _LOCAL_FIELDS['L']]

  # insert
  df = pd.concat([df, L], axis = 1)

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def insert_zenith(
  df: pd.DataFrame, n_jobs: int = -1, progress_kwargs: bool = True
) -> pd.DataFrame:
  '''
  Insert zenith angles.

  Parameters:
  - df: input dataframe; it must contain orientations
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Return:
  dataframe
  '''

  logger = _LOGGER.getChild('insert_zenith')

  logger.debug(
    f'args: dataframe shape: {df.shape}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  # zenith
  z = _insert(
    df.loc[:, [_LOCAL_FIELDS[key] for key in ['pitch', 'roll', 'yaw']]],
    func = get_zenith, name = _LOCAL_FIELDS['zenith'], n_jobs = n_jobs,
    progress_kwargs = progress_kwargs
  )
  z = z.loc[:, _LOCAL_FIELDS['zenith']]

  # insert
  df = pd.concat([df, z], axis = 1)

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def insert_corr_rates(
  df: pd.DataFrame, rates: list = _DEFAULT_CORR_RATES, n_jobs: int = -1,
  progress_kwargs: Optional[dict] = None
) -> pd.DataFrame:
  '''
  Insert rates divided by livetime.

  Parameters:
  - df: input dataframe
  - rates: rates to be divided by livetime
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Return:
  dataframe
  '''

  logger = _LOGGER.getChild('insert_corr_rates')

  logger.debug(
    f'args: dataframe shape: {df.shape}, rates: {rates}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  # insert

  for rate in tqdm(rates, **_get_progress_kwargs(progress_kwargs)):

    # ignore missing rates
    if rate not in df.columns.tolist():
      logger.info(
        f'Rate {rate} is missing, corrected form will not be inserted'
      )
      continue

    col = f'{rate}_{_RATES_SUFFIXES["corr"]}'

    x = _insert(
      df.loc[:, [rate, _LOCAL_FIELDS['livetime']]],
      func = lambda x, y : x / y if y != 0 else np.nan, name = col,
      ffill = False, n_jobs = n_jobs, progress_kwargs = {'disable': True}
    )

    df = pd.concat([df, x.loc[:, col]], axis = 1)

    logger.debug(
      f'description of new column {col}: '
      f'\n{x.loc[:, col].describe(datetime_is_numeric = True).to_string()}'
    )

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def insert_rates_ratios(
  df: pd.DataFrame, rates: dict = _DEFAULT_RATES_RATIOS, n_jobs: int = -1,
  progress_kwargs: bool = True
) -> pd.DataFrame:
  '''
  Insert rates ratios.

  Parameters:
  - df: input dataframe
  - rates: dict containing {rate1: rate2} pairs to get rate1 / rate2 ratio
  - n_jobs: parallel jobs to run (default: nproc; look at `joblib.Parallel`
    reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Return:
  dataframe
  '''

  logger = _LOGGER.getChild('insert_rates_ratios')

  logger.debug(
    f'args: dataframe shape: {df.shape}, rates: {rates}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  # insert

  for r1, r2 in rates.items():

    # ignore missing rates
    if r1 not in df.columns.tolist() or r2 not in df.columns.tolist():
      logger.info(
        f'At least one of rates {r1}, {r2} is missing, ratio will not be '
        'inserted'
      )
      continue

    col = f'{r1}_{r2}_{_RATES_SUFFIXES["ratio"]}'

    x = _insert(
      df.loc[:, [r1, r2]], func = lambda x, y : x / y if y != 0 else np.nan,
      name = col, ffill = False, n_jobs = n_jobs,
      progress_kwargs = progress_kwargs
    )

    df = pd.concat([df, x.loc[:, col]], axis = 1)

    logger.debug(
      f'description of new column {col}: '
      f'\n{x.loc[:, col].describe(datetime_is_numeric = True).to_string()}'
    )

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df


def preprocess(
  positions: pd.DataFrame, orientations: pd.DataFrame, features: pd.DataFrame,
  corr_rates: list = _DEFAULT_CORR_RATES,
  rates_ratios: dict = _DEFAULT_RATES_RATIOS, n_jobs: int = -1,
  progress_kwargs: bool = True
) -> pd.DataFrame:
  '''
  Preprocess AMS data for SEP monitoring.

  Parameters:
  - positions: dataframe with cartesian positions
  - orientations: dataframe with orientations
  - features: dataframe with features
  - corr_rates: rates to be divided by livetime
  - rates_ratios: rates ratios to insert, passed as `{rate1: rate2}` to get
    `rate1 / rate2`
  - n_jobs: number of jobs for parallelized operations (default: nproc; look at
    `joblib.Parallel` reference for more info)
  - progress_kwargs: kwargs for progress bars (i.e. `tqdm` objects)

  Returns:
  preprocessed dataframe
  '''

  logger = _LOGGER.getChild('preprocess')

  logger.debug(
    f'args: positions shape: {positions.shape}, '
    f'orientations shape: {orientations.shape}, '
    f'features shape: {features.shape}, corr_rates: {corr_rates}, '
    f'rates_ratios: {rates_ratios}, n_jobs: {n_jobs}, '
    f'progress_kwargs: {progress_kwargs}'
  )

  # store columns
  pos_cols = positions.columns.tolist()
  orient_cols = orientations.columns.tolist()
  feat_cols = features.columns.tolist()

  # concat inputs
  df = pd.concat(
    [x.reset_index() for x in [positions, orientations, features]],
    ignore_index = True
  )
  df = df.set_index(_LOCAL_FIELDS['epoch'])
  df = compact_records(df)

  ## preprocess

  # convert meters to earth radii
  df.loc[:, pos_cols] = df.loc[:, pos_cols].applymap(get_earth_radii)

  # convert livetime from [0,100] to [0,1]
  df.loc[:, _LOCAL_FIELDS['livetime']] /= 100

  # get keep mask for null values

  for key, func in {
    'position': insert_pos_age,
    'orientation': insert_orient_age
  }.items():

    p = progress_kwargs.copy()
    p.update({'desc': f'Inserting {key} age'})

    df = func(df, n_jobs, p)

  # fill null values
  cols = pos_cols + orient_cols
  df.loc[:, cols] = df.loc[:, cols].fillna(method = 'ffill')

  # drop records with null positions or orientations
  # (L, B and zenith can not be evaluated)

  null_mask = df.loc[:, pos_cols + orient_cols].isna().any(axis = 1)

  if null_mask.any():
    desc = df.loc[null_mask].reset_index().describe(datetime_is_numeric = True)
    logger.info(
      'Records lost because of null positions or orientations: '
      f'\n{desc.to_string()}'
    )
    df = df.loc[null_mask == False]

  # drop records that has no feature

  null_mask = df.loc[:, feat_cols].isna().all(axis = 1)

  if null_mask.any():
    desc = df.loc[null_mask].reset_index().describe(datetime_is_numeric = True)
    logger.info(
      f'Records lost because of missing features: \n{desc.to_string()}'
    )
    df = df.loc[null_mask == False]

  # insert B, L, zenith, corrected rates and rates ratios

  for key, func in {
    'B': insert_B, 'zenith': insert_zenith, 'L': insert_L,
    'corrected rates': partial(insert_corr_rates, rates = corr_rates),
    'rates ratios': partial(insert_rates_ratios, rates = rates_ratios)
  }.items():

    p = progress_kwargs.copy()
    p.update({'desc': f'Inserting {key}'})

    df = func(df, n_jobs = n_jobs, progress_kwargs = p)

  # reorder columns
  new_cols = \
    df.drop(columns = pos_cols + orient_cols + feat_cols).columns.tolist()
  df = df.loc[:, pos_cols + orient_cols + new_cols + feat_cols]

  logger.debug(f'dataframe shape (return): {df.shape}')

  return df
