import logging
import os
from logging import getLogger

_LOGGER = getLogger().getChild(__name__)


# disable multi-threading for CERN batch service
try:
  import scipy.stats
except KeyboardInterrupt:
  os.environ['OPENBLAS_NUM_THREADS'] = '0'
  _LOGGER.error('Received SIGINT while trying to import scipy.stats: openblas threads disabled.')


import warnings
from warnings import warn
import json
import sqlite3
from argparse import ArgumentParser
from time import sleep
from pathlib import Path
import pandas as pd
import numpy as np
from scipy.stats import poisson, norm
from tqdm import trange
from .helpers import \
  _LOCAL_FIELDS, _RATES_SUFFIXES, _PROGRESS_KWARGS, _get_progress_kwargs
from .helpers.database import write as write_on_remote
from .helpers.database import \
  _DEFAULT_REMOTE, get_car_positions, get_orientations, get_features
from .helpers.preprocessing import \
  _DEFAULT_CORR_RATES, _DEFAULT_RATES_RATIOS, preprocess
from .helpers.scoring import _BINNED, Analyzer


# suppress future warnings
warnings.simplefilter(action = 'ignore', category = FutureWarning)


# default options values

_DEFAULT_JOBS = -1
_DEFAULT_BUFFER_LENGTH = 3*24*60*60 # seconds = 3 days
_DEFAULT_SCORE_THRESH = 1e-6
_DEFAULT_B_THRESH = 25000 # nT
_DEFAULT_ZENITH_THRESH = 15 # deg
_DEFAULT_POS_AGE_THRESH = 10 # s
_DEFAULT_ORIENT_AGE_THRESH = 60 # s
_DEFAULT_BINS_RES = 0.5
_DEFAULT_FEATURES = {}
_DEFAULT_LIVE_PERIOD = 60 # s
_DEFAULT_LIVE_DELAY = 0 # s
_DEFAULT_MEAS = 'sep'

# corr rates
for key in [
  'LV1', 'FT', 'FTC', 'FTE', 'subLV1_0', 'subLV1_1', 'subLV1_2', 'subLV1_4',
  'subLV1_5'
]:
  _DEFAULT_FEATURES[f"{_LOCAL_FIELDS[key]}_{_RATES_SUFFIXES['corr']}"] = \
    'norm'

# rates
for key in ['FTZ', 'subLV1_3']:
  _DEFAULT_FEATURES[_LOCAL_FIELDS[key]] = 'poisson'

# scoring parameters
## time range of one chunk in the past
_PAST_CHUNKS_DELTA = pd.Timedelta(days = 7)
## epsilon between the end of a chunk and the start of the following one
_EPSILON_CHUNKS = pd.Timedelta(seconds = 0.001)


# log glob vars
_LOGGER.debug(f'default jobs: {_DEFAULT_JOBS}')
_LOGGER.debug(f'default buffer length: {_DEFAULT_BUFFER_LENGTH}')
_LOGGER.debug(f'default score tresh: {_DEFAULT_SCORE_THRESH}')
_LOGGER.debug(f'default B tresh: {_DEFAULT_B_THRESH}')
_LOGGER.debug(f'default zenith tresh: {_DEFAULT_ZENITH_THRESH}')
_LOGGER.debug(f'default thresh for position age: {_DEFAULT_POS_AGE_THRESH}')
_LOGGER.debug(f'default thresh for orientation age: {_DEFAULT_ORIENT_AGE_THRESH}')
_LOGGER.debug(f'default bins resolution: {_DEFAULT_BINS_RES}')
_LOGGER.debug(f'default features and scoring distribution: {_DEFAULT_FEATURES}')
_LOGGER.debug(f'default live period: {_DEFAULT_LIVE_PERIOD}')
_LOGGER.debug(f'default live delay: {_DEFAULT_LIVE_DELAY}')
_LOGGER.debug(f'default measurement: {_DEFAULT_MEAS}')
_LOGGER.debug(f'past chunks delta: {_PAST_CHUNKS_DELTA}')
_LOGGER.debug(f'epsilon between chunks: {_EPSILON_CHUNKS}')


def save(df: pd.DataFrame, label: str, args) -> None:
  '''
  Save output.

  Parameters:
  - df: output dataframe
  - label: data label for sqlite table (local output) or InfluxDB measurement
    (remote output) names
  - args: CLI args
  '''

  logger = _LOGGER.getChild('save')

  logger.debug(
    f'args: dataframe shape: {df.shape}, label: {label}, args: {args}'
  )

  if args.output_subcommand == 'local':
    with sqlite3.connect(args.path) as con:
      df.to_sql(label, con, if_exists = 'append')
  elif args.output_subcommand == 'remote':
    write_on_remote(
      args.database, label, df,
      remote = args.output_remote if args.output_remote is not None \
        else args.remote,
      username = args.output_username if args.output_username is not None \
        else args.username,
      password = args.output_password if args.output_password is not None \
        else args.password,
      n_jobs = args.jobs,
      progress_kwargs = {'disable': True}
    )


def main(args) -> None:
  '''
  Score AMS data for SEP detection.

  Parameters:
  - args: CLI args
  '''

  logger = _LOGGER.getChild('main')

  logger.debug(f'args: {args}')

  # log analysis parameters
  logger.info(f'{_BINNED} bins resolution: {args.bins_res}')

  # parse corr rates and rates ratios
  corr_rates = args.corr_rates.split(',')
  rates_ratios = json.loads(args.rates_ratios)

  logger.info(f'Corrected rates: {corr_rates}')
  logger.info(f'Rates ratios: {json.dumps(rates_ratios, indent = 2)}')

  # parse features
  features = json.loads(args.features)

  logger.info(
    f'Features and scoring distributions: {json.dumps(features, indent = 2)}'
  )

  # columns for scoring analyzer
  analyzer_cols = [_BINNED] + list(features.keys())

  logger.debug(f'columns for scoring analyzer: {analyzer_cols}')

  # parse buffer length
  buffer_length = pd.Timedelta(seconds = args.buffer_length)

  logger.info(f'Buffer length: {buffer_length}')

  # log buffer threshs
  for key, value in {
    'score': args.score_thresh,
    'B': args.B_thresh,
    'zenith': args.zenith_thresh,
    'position age': args.pos_age_thresh,
    'orientation age': args.orient_age_thresh
  }.items():

    logger.info(f'{key.capitalize()} threshold for buffer: {value}')

  # warnings
  if buffer_length > _PAST_CHUNKS_DELTA:
    warn(
      'The use of a big buffer length could slow down execution or saturate '
      f'system\'s memory. It is suggested to not exceeed {_PAST_CHUNKS_DELTA}.'
    )

  # parse live period and delay
  live_period = pd.Timedelta(seconds = args.live_period)
  live_delay = pd.Timedelta(seconds = args.live_delay)

  logger.info(f'Period for live data queries: {live_period}')
  logger.info(f'Delay for live data queries: {live_delay}')

  # get now
  now = pd.Timestamp.now('utc')

  # parse timestamps

  start = now if args.start is None else pd.Timestamp(args.start)

  if args.length is not None:
    end = start + pd.Timedelta(seconds = args.length)
  elif args.end is None:
    end = pd.Timestamp.max
  else:
    end = pd.Timestamp(args.end)

  if start.tz is None: start = start.tz_localize('utc')
  if end.tz is None: end = end.tz_localize('utc')

  start -= buffer_length

  logger.info(f'Processing data from {start} to {end}')

  # data analyzer end is initialized to 0 so it will be adjusted by first buffer
  analyzer = Analyzer(args.bins_res, buffer_length, pd.Timestamp(0))

  t = start # current step
  prev_last_valid = [None]*2 # last valid pos and orient from the prev chunk

  # print progress msg
  print_msg = lambda s: print(s, file = _PROGRESS_KWARGS['file']) \
    if args.verbose or not args.quiet else None

  while t < end:

    now = pd.Timestamp.now('utc')
    now_delayed = now - live_delay
    chunk_start = t
    chunk_end = t

    if t == start: # no scoring, only buffering fill
      chunk_end += buffer_length
    elif now_delayed - t > live_period:
      chunk_end += np.min([_PAST_CHUNKS_DELTA, now_delayed - t])
    else:
      chunk_end += live_period

    chunk_end -= _EPSILON_CHUNKS

    if chunk_end > end:
      # reset chunk end to global end: this will be the last chunk
      chunk_end = end

    logger.debug(
      f'now: {now}, now_delayed: {now_delayed}, chunk_start: {chunk_start}, '
      f'chunk_end: {chunk_end}'
    )

    # wait for the data to be available
    if chunk_end > now_delayed:

      wait = chunk_end - now_delayed

      print_msg(f'Waiting for {now + wait} for next query...')
      logger.debug(f'sleeping for {wait} until {now + wait}')

      sleep(wait.total_seconds())
      logger.debug(f'waking up (now: {pd.Timestamp.now()})')

    print_msg(f'Querying chunk of data from {chunk_start} to {chunk_end}...')

    chunk = [
      func(chunk_start, chunk_end, args.remote, args.username, args.password)
      for func in [get_car_positions, get_orientations, get_features]
    ]

    # save last valid values to be injected at the next iteration and inject the
    # one's saved from the previous one

    last_valid = []

    for i in range(len(prev_last_valid)):

      # inject last valid position and orientation from the previous chunk
      if prev_last_valid[i] is not None:
        chunk[i] = pd.concat([prev_last_valid[i], chunk[i]])
        logger.debug(
          f'injected data from the previous chunk: {prev_last_valid[i]}'
        )

      # update last valid pos and orient for the next iteration
      # (they could also be the same ones of the previous chunk, but it is ok,
      # preprocessing ffill limits will limit the ffill anyway)
      try:
        prev_last_valid[i] = chunk[i].dropna().iloc[[-1]]
      except IndexError as ex:
        # no valid value available
        logger.debug(
          f'no valid value available as last for {["pos", "orient"][i]} in the '
          'current chunk'
        )
        prev_last_valid[i] = None

    print_msg('Preprocessing chunk...')

    # preprocess chunk
    df_preprocessed = preprocess(
      *chunk, corr_rates, rates_ratios, n_jobs = args.jobs,
      progress_kwargs = {'disable': not args.verbose}
    )

    # sort chronologically
    df_preprocessed.sort_index(inplace = True)

    # get keep mask of buffer thresholds
    buffer_mask = pd.DataFrame(
      {
        key: df_preprocessed.loc[:, _LOCAL_FIELDS[key]] <= thresh
        for key, thresh in {
          'zenith': args.zenith_thresh,
          'pos_age': args.pos_age_thresh,
          'orient_age': args.orient_age_thresh
        }.items()
      },
      index = df_preprocessed.index
    )
    buffer_mask.insert(
      0, _LOCAL_FIELDS['B'],
      df_preprocessed.loc[:, _LOCAL_FIELDS['B']] >= args.B_thresh
    )

    if t == start: # fill the buffer without scoring

      # indexes of records to buffer
      buffer_idx = buffer_mask.all(axis = 1).loc[lambda x: x].index.to_series()

      logger.info(
        f'Properties of buffered records: count: {buffer_idx.shape[0]}, '
        f'length: {buffer_idx.max() - buffer_idx.min()}'
      )

      analyzer.buffer(df_preprocessed.loc[buffer_idx, analyzer_cols])

      n = 0 # records to score

      print_msg('Chunk buffered.')

    else: # score the preprocessed chunk

      n = df_preprocessed.shape[0] # records to score

      if n == 0:
        print_msg('No data to score in chunk.')

    logger.debug(f'records to score: {n}')

    for i in trange(n, **_get_progress_kwargs({
      'desc': 'Scoring chunk',
      'disable': n == 0 or args.quiet and not args.verbose
    })):

      # score
      df_scored = analyzer.score(
        df_preprocessed.iloc[[0]].filter(analyzer_cols), n_jobs = args.jobs,
        dist = features
      )

      # build output dataframe from analyzer output and other preprocessing data
      df_output = df_preprocessed.iloc[[0]].drop(
        columns = df_scored.columns.get_level_values('feature').tolist()
      )
      df_output.columns = pd.MultiIndex.from_product(
        [df_output.columns.tolist(), ['value']], names = df_scored.columns.names
      )
      df_output = pd.concat([df_scored, df_output], axis = 1)

      # append bin to index
      df_output = df_output.set_index((_BINNED, 'bin'), append = True)
      df_output.index.names = \
        ['_'.join(s) if type(s) == tuple else s for s in df_output.index.names]

      # check score thresh
      score_under_thresh = df_scored.loc[:, (slice(None), 'score')].droplevel(1, axis = 1)
      score_under_thresh = score_under_thresh.applymap(lambda x: x < args.score_thresh) # nans give false

      # cuts indicators shows if a record meets buffer thresholds requirements
      # (0) or not (1); score thresh is not considered
      cuts_indicators = buffer_mask.iloc[[0]].applymap(lambda x: int(not x))
      cuts_indicators = cuts_indicators.rename(columns = lambda s: f'{s}_cut')

      # append cuts indicators to output index
      df_output.index = pd.MultiIndex.from_frame(
        df_output.index.to_frame().join(cuts_indicators)
      )

      # stack features
      df_output = df_output.stack('feature', dropna = False)

      # save output
      try:
        save(df_output, args.meas, args)
      except Exception as ex:
        raise RuntimeError(
          f'unable to save scored data for chunk {chunk_start} -- {chunk_end}'
          f'(failure at {df_preprocessed.iloc[0].name})'
        ) from ex

      # swap multi-index levels to (value_type, feature)
      df_scored = df_scored.swaplevel(axis = 1)

      # buffer data
      if buffer_mask.loc[df_preprocessed.iloc[0].name].all():

        # remove data under score thresh
        cols = [('value', col) for col in score_under_thresh.columns.tolist()]
        df_scored.loc[:, cols] = df_scored.loc[:, cols].where(score_under_thresh.applymap(lambda x: not x), np.nan)

        # append to buffer
        analyzer.buffer(df_scored.loc[:, 'value'])

      # update data to score
      df_preprocessed = df_preprocessed.iloc[1:]

      # drop no more needed masks and restore the original columns (i.e. drop
      # score)
      buffer_mask = buffer_mask.iloc[1:]

    # update timestep
    t = chunk_end + _EPSILON_CHUNKS


if __name__ == '__main__':

  ## command-line interface

  parser = ArgumentParser()

  # general options
  parser.add_argument(
    '-D', '--debug', action = 'store_true', help = 'print debug logs'
  )
  parser.add_argument(
    '-q', '--quiet', action = 'store_true', help = 'suppress progress info'
  )
  parser.add_argument(
    '-v', '--verbose', action = 'store_true',
    help = 'increase log level to INFO and print more information about ' + \
      'lost records properties'
  )
  parser.add_argument('--logfile', type = str, default = None)
  parser.add_argument(
    '-j', '--jobs', type = int, default = _DEFAULT_JOBS,
    help = 'parallel jobs to run (look at `joblib.Parallel` documentation ' + \
      f'for more information; default: {_DEFAULT_JOBS})'
  )

  # remote InfluxDB options
  parser.add_argument(
    '-r', '--remote', type = str, default = _DEFAULT_REMOTE,
    help = 'remote InfluxDB'
  )
  parser.add_argument(
    '-u', '--username', type = str, help = 'remote InfluxDB username'
  )
  parser.add_argument(
    '-p', '--password', type = str, help = 'remote InfluxDB password'
  )

  # time range options

  parser.add_argument(
    '-s', '--start', type = str,
    help = 'start timestamp of scoring (default: now)'
  )

  group = parser.add_mutually_exclusive_group()

  group.add_argument(
    '-e', '--end', type = str,
    help = 'end timestamp of scoring (default: never)'
  )
  group.add_argument(
    '-l', '--length', type = float,
    help = 'time duration of scoring expressed in seconds; this option ' + \
      'replaces --end (default: end - start)'
  )

  # corrected rates and rates ratios options
  parser.add_argument(
    '--corr-rates', type = str, default = ','.join(_DEFAULT_CORR_RATES),
    help = 'divide the original rate by livetime and insert the new column ' + \
      f'in data (default: {_DEFAULT_CORR_RATES})'
  )
  parser.add_argument(
    '--rates-ratios', type = str, default = json.dumps(_DEFAULT_RATES_RATIOS),
    help = 'dict containing the pairs of rates to be divided (e.g. ' + \
      '{rate1: rate2} generates rate1 / rate2 values; ' + \
      f'default: {_DEFAULT_RATES_RATIOS})'
  )

  # buffer options
  parser.add_argument(
    '--buffer-length', type = float, default = _DEFAULT_BUFFER_LENGTH,
    help = 'maximum time window used for past data buffering and reference ' + \
      f'distributions (default: {_DEFAULT_BUFFER_LENGTH})'
  )
  parser.add_argument(
    '--score-thresh', type = float, default = _DEFAULT_SCORE_THRESH,
    help = 'score threshold for past data buffering; records with scores ' + \
      'lower than this value will not be used in reference distributions ' + \
      f'(default: {_DEFAULT_SCORE_THRESH})'
  )
  parser.add_argument(
    '--B-thresh', type = float, default = _DEFAULT_B_THRESH,
    help = 'geomagnetic field intensity threshold for past data buffering; ' + \
      'records with B intensities lower than this value will not be used ' + \
      f'in reference distributions (units: nT; default: {_DEFAULT_B_THRESH})'
  )
  parser.add_argument(
    '--zenith-thresh', type = float, default = _DEFAULT_ZENITH_THRESH,
    help = 'zenith angle threshold for past data buffering; records with ' + \
      'zenith angles greater than this value will not be used in reference ' + \
      f'distributions (units: deg; default: {_DEFAULT_ZENITH_THRESH})'
  )
  parser.add_argument(
    '--pos-age-thresh', type = float,
    default = _DEFAULT_POS_AGE_THRESH,
    help = 'position data age threshold for past data ' + \
      'buffering; records with a position missing for more than the ' + \
      'specified time will not be used in reference distributions (units: ' + \
      f'seconds; default: {_DEFAULT_POS_AGE_THRESH})'
  )
  parser.add_argument(
    '--orient-age-thresh', type = float,
    default = _DEFAULT_ORIENT_AGE_THRESH,
    help = 'orientation data age threshold for past data ' + \
      'buffering; records with ISS flight angles missing for more than the ' + \
      'specified time will not be used in reference distributions (units: ' + \
      f'seconds; default: {_DEFAULT_ORIENT_AGE_THRESH})'
  )

  # L bins options
  parser.add_argument(
    '--bins-res', type = float, default = _DEFAULT_BINS_RES,
    help = f'{_BINNED} bins resolution (default: {_DEFAULT_BINS_RES})'
  )

  # scoring features and distributions option
  parser.add_argument(
    '--features', type = str, default = json.dumps(_DEFAULT_FEATURES),
    help = 'dictionary with features and scoring distributions, passed ' + \
      "as {'feature': 'distribution'} (supported distributions: " + \
      f"'norm', 'poisson'; default: {json.dumps(_DEFAULT_FEATURES)})"
  )

  # realtime processing options
  parser.add_argument(
    '--live-period', type = float, default = _DEFAULT_LIVE_PERIOD,
    help = 'period of live data queries (units: seconds; default: ' + \
      f'{_DEFAULT_LIVE_PERIOD})'
  )
  parser.add_argument(
    '--live-delay', type = float, default = _DEFAULT_LIVE_DELAY,
    help = 'do not process data on the real-time edge but introduce a ' + \
      f'delay (units: seconds; default: {_DEFAULT_LIVE_DELAY})'
  )

  # measurement option
  parser.add_argument(
    '--meas', type = str, default = _DEFAULT_MEAS,
    help = 'label used as sqlite table name for local output or InfluxDB ' + \
      f'measurement for remote output (default: {_DEFAULT_MEAS})'
  )

  # output subcommands

  subparsers = parser.add_subparsers(dest = 'output_subcommand')

  for cmd, h in {
    'local': 'save output in a local sqlite file',
    'remote': 'save output in a remote InfluxDB'
  }.items():

    p = subparsers.add_parser(cmd, help = h)

    # sqlite local output
    if cmd == 'local':
      p.add_argument('path', type = str, help = 'sqlite output file')

    # remote InfluxDB output
    elif cmd == 'remote':

      # remote InfluxDB options (for output data)
      p.add_argument(
        '-r', '--remote', type = str, dest = 'output_remote',
        default = _DEFAULT_REMOTE,
        help = 'remote InfluxDB (for output data; default: value passed ' + \
          'with global options)'
      )
      p.add_argument(
        '-u', '--username', type = str, dest = 'output_username',
        help = 'remote InfluxDB username (for output data; default: value ' + \
          'passed with global options)'
      )
      p.add_argument(
        '-p', '--password', type = str, dest = 'output_password',
        help = 'remote InfluxDB password (for output data; default: value ' + \
          'passed with global options)'
      )

      p.add_argument('database', type = str, help = 'output InfluxDB database')

  ## read args

  args = parser.parse_args()

  ## logging

  log_level = None
  log_formatter = None
  log_handler = None

  # define level and formatter (only if debug or verbose mode are active)
  if args.debug:
    log_level = logging.DEBUG
    log_formatter = logging.Formatter(
      '[{levelname: ^8}] -- {asctime} -- {name}: {message}', style = '{'
    )
  elif args.verbose:
    log_level = logging.INFO
    log_formatter = logging.Formatter('{message}', style = '{')

  # define handler
  # (only if a logfile is given or debug or verbose modes are active)
  if args.logfile is not None:
    log_handler = logging.FileHandler(args.logfile)
  elif args.debug or args.verbose:
    log_handler = logging.StreamHandler()

  # set handler's formatter
  if log_handler is not None and log_formatter is not None:
    log_handler.setFormatter(log_formatter)

  # set root logger's level
  if log_level is not None:
    logging.getLogger().setLevel(log_level)

  # set root logger's handler
  if log_handler is not None:
    logging.getLogger().addHandler(log_handler)

  ## execute

  logging.debug(f'args: {args}')

  main(args)
