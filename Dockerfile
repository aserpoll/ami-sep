FROM ubuntu:22.10

ARG DEBIAN_FRONTEND=noninteractive
ARG GRAFANA_VERSION=8.5.20
ARG INFLUXDB_VERSION=1.8*

WORKDIR /usr/local/src/app

# needed to setup grafana and influxdb repos
RUN set -e; \
  apt update; \
  apt install -y gpg wget

# grafana setup

RUN set -e; \
  apt update; \
  apt install -y apt-transport-https software-properties-common; \
  wget -q -O - https://packages.grafana.com/gpg.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/grafana.gpg >/dev/null; \
  echo "deb https://packages.grafana.com/enterprise/deb stable main" | tee /etc/apt/sources.list.d/grafana.list >/dev/null

RUN set -e; \
  apt update; \
  apt install -y "grafana=${GRAFANA_VERSION}"

ENV GF_SECURITY_USER=admin
ENV GF_SECURITY_PASSWORD=admin

# influxdb setup

RUN set -e; \
  wget -q https://repos.influxdata.com/influxdata-archive_compat.key; \
  echo '393e8779c89ac8d958f81f942f9ad7fb82a25e133faddaf92e15b16e6ac9ce4c influxdata-archive_compat.key' | sha256sum -c && cat influxdata-archive_compat.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg >/dev/null; \
  rm influxdata-archive_compat.key; \
  echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdata-archive_compat.gpg] https://repos.influxdata.com/debian stable main' | tee /etc/apt/sources.list.d/influxdata.list >/dev/null

RUN set -e; \
  apt update; \
  apt install -y "influxdb=${INFLUXDB_VERSION}"

ENV INFLUXDB_DB=main
ENV INFLUXDB_HTTP_ENABLED=true
ENV INFLUXDB_HTTP_AUTH_ENABLED=false
ENV INFLUXDB_LOGFILE=/var/log/influxdb.log

# app setup

# NumPy is installed before other requirements to avoid SpacePy installation's
# failure, then the one is installed system-wide is used without taking into
# consideration the version specified in `requirements.txt`
RUN set -e; \
  apt update; \
  apt install -y gfortran python3 python3-numpy python3-pip

COPY requirements.txt .

RUN set -e; \
  grep -v numpy requirements.txt >requirements.new.txt; \
  mv requirements.new.txt requirements.txt; \
  python3 -m pip install --no-cache-dir --no-input -r requirements.txt

COPY . .

# make app dir editable by anyone
RUN chmod -R a=rwx .

ENV PYTHONPATH=/usr/local/src/app/src
ENV REQUESTS_CA_BUNDLE=/usr/local/src/app/certs/ca-bundle.crt

# avoid permission errors with non-root users
ENV SPACEPY=/usr/local/src/app/.spacepy

# expose grafana and influxd port
EXPOSE 3000
EXPOSE 8086

ENTRYPOINT ["/usr/local/src/app/docker-entrypoint.sh"]
CMD ["-h"]
