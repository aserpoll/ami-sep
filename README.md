# AMI SEP

## Install

The following instructions are intended for Linux systems.

To install the project the `requirements.txt` file can be used or a Docker
image can be built to run the app in a container or as a service.
In both cases this repository needs to be cloned using `git`.
```
git clone <repository url>
```

The project is tested on Python 3.10.
Other versions of the interpreter could work fine as well, but it is not
granted.

### Local installation

The requirements can be installed running
```
python3.10 -m pip install -r requirements.txt
```
from the cloned folder.

### Dockerfile

To build a Docker image run
```
docker build -t ami-sep .
```
from the cloned folder.

## Preliminary steps

The project needs CERN CA certificates and an access to CERN network.

### SSL certificates

A ready-to-use bundle is included in the project.
That can be used setting the environment variable `REQUESTS_CA_BUNDLE`:
```
# prefer an absolute path rather than a relative path
export REQUESTS_CA_BUNDLE="<path to the cloned folder>/certs/ca-bundle.crt"
```

### CERN network

CERN network can be accessed from outside using SSH tunneling with an active
computing account:
```
ssh -D 8080 <username>@lxtunnel.cern.ch
# insert password when asked

export HTTP_PROXY=socks5h://localhost:8080
export HTTPS_PROXY=socks5h://localhost:8080
export NO_PROXY=localhost
```
**Note**: the `h` in the proxy protocol is needed to proxy hostnames resolution
as well.

For more information the CERN guide for SSH tunneling is available
[here](https://abpcomputing.web.cern.ch/guides/sshtunnel/).

## Usage

### Local installation

The `src` directory can be appended to `PYTHONPATH` to avoid changing the
current directory to `src` everytime:
```
export PYTHONPATH="$PYTHONPATH:<path to the project folder>/src"
```
The project can be run as a python module (i.e. using `-m` option):
```
python3.10 -m ami_sep <options> <args>
```

The module and its subcommands provide working help options:
```
python3.10 -m ami_sep [-h | --help]
```

### Dockerfile

The project can be run in a container:
```
docker run ami-sep <options> <args>
```
with `options` and `args` the same accepted by the module.

Docker containers are not persistent; a file or a directory can be mounted from
the host machine to save the output data:
```
# create a file
sudo touch data.sqlite

docker run -v $PWD/data.sqlite:/data.sqlite ami-sep local /data.sqlite
```

#### InfluxDB and Grafana support

Differently from local usage, docker containers provide also InfluxDB and
Grafana.
They are disabled by default and can be enabled setting `ENABLE_INFLUXDB` and
`ENABLE_GRAFANA`:
```
docker run \
  --env ENABLE_INFLUXDB=true \
  --env ENABLE_GRAFANA=true \
  ami-sep <options> <args>
```

After `ami-sep` operations have been completed, the container would stop.
To keep the container alive and continue accessing InfluxDB or Grafana, set
`KEEP_ALIVE`:
```
docker run \
  --env ENABLE_INFLUXDB=true \
  --env ENABLE_GRAFANA=true \
  --env KEEP_ALIVE=true \
  ami-sep <options> <args>
```
Then the container can be stopped with `docker stop`.
Consider also detaching the container using the option `-d` of `docker run`.

To provide persistance of Grafana and InfluxDB mount external directories for
`/var/lib/influxdb` and `/var/lib/grafana`:
```
docker run \
  --env ENABLE_INFLUXDB=true \
  --env ENABLE_GRAFANA=true \
  -v $PWD/influxdb:/var/lib/influxdb \
  -v $PWD/grafana:/var/lib/grafana \
  ami-sep <options> <args>
```

#### CERN network

Also docker containers need access to CERN network.
In this case the easiest way to get it is to open an SSH tunnel on the host
machine and make the container use its network:
```
ssh -D 8080 <username>@lxtunnel.cern.ch
# insert password when asked

docker run \
  --net=host \
  --env HTTP_PROXY=socks5h://localhost:8080 \
  --env HTTPS_PROXY=socks5h://localhost:8080 \
  --env NO_PROXY=localhost \
  ami-sep <options> <args>
```
**Note**: this way the docker container will have complete access to the
network of the host and this is why `--net=host` is usually not recommended.
