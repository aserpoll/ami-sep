#!/bin/bash

sleep_time=5

# start grafana and wait (it gives a fake fail status)
if [[ $ENABLE_GRAFANA = true ]]; then
  echo -n 'Starting Grafana... '
  service grafana-server start 2>&1 >/dev/null
  sleep $sleep_time
fi

set -e

# check grafana is actually running
if [[ $ENABLE_GRAFANA = true ]]; then
  service grafana-server start 2>&1 >/dev/null
  echo OK.
fi

# start influxd

if [[ -z $INFLUXDB_LOGFILE ]]; then
  export INFLUXDB_LOGFILE=/dev/null
fi

if [[ $ENABLE_INFLUXDB = true ]]; then
  echo -n 'Starting influxd... '
  influxd 2>$INFLUXDB_LOGFILE >/dev/null & disown
  sleep $sleep_time
  echo OK.
fi

# create database
if [[ $ENABLE_INFLUXDB = true ]] && [[ -n $INFLUXDB_DB ]]; then
  echo -n "Creating database '$INFLUXDB_DB'... "
  curl -sS -i -XPOST http://localhost:8086/query \
      --data-urlencode "q=CREATE DATABASE $INFLUXDB_DB" \
      >/dev/null
  echo OK.
fi

# propagate args to the app
python3 -m ami_sep "$@"

# keep container alive
if [[ $KEEP_ALIVE = true ]]; then
  echo 'Keeping container alive...'
  sleep infinity
fi
