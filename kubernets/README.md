# Deploy on Kubernets

`kubernets/` directory provides `yaml` files that can be used to configure and
deploy a series of fillers for an InfluxDB database using a Kubernets cluster
(e.g. [CERN PaaS service](https://paas.docs.cern.ch/)).

The directory contains:
- `*-credentials.yml*`, `Secrets` configs for InfluxDB databases credentials,
  one for the official AMS Monitoring Interface database and one for a
  separate database filled with processed data;
- `ami-sep-db.yml`, `ConfigMap` config for URL and database name of the
  InfluxDB database to fill with processed data;
- `fillers.yml`, `Deployment` config to deploy a series of fillers to
  process data in realtime.

The deployed fillers use a variable and configurable delay in respect to the
present time.

**Note**: `Secret` and `ConfigMap` config files must be filled with actual
values before deployment.

